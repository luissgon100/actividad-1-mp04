# Requisitos del Proyecto

Este proyecto tiene como objetivo implementar ciertos requisitos específicos en una tabla HTML para mejorar su estilo y funcionalidad.

## Requerimientos

- Añadir un efecto hover a la tabla.
- Utilizar 3 tipos de fuentes de Google Fonts:
  - Una para el encabezado h1 (*Detalles del Teléfono*).
  - Otra para el encabezado de las filas (*Teléfono*, *Capacidad*, etc.).
  - La tercera para el texto de la tabla.
- Utilizar pseudoclases para añadir un color a las líneas impares de la tabla.

## Detalles de Implementación

### Efecto Hover en la Tabla:

Para añadir un efecto hover a la tabla, se puede utilizar CSS y aplicar estilos específicos al elemento `tr` cuando se coloca el cursor sobre él.

### Fuentes de Google Fonts:

- Se deben importar las fuentes deseadas desde Google Fonts en el archivo HTML.
- Para el encabezado h1, se puede aplicar la primera fuente importada.
- Para el encabezado de las filas y el texto de la tabla, se pueden aplicar las otras dos fuentes respectivamente.
